# rubocop:disable Metrics/LineLength, Metrics/MethodLength, Metrics/ModuleLength

# Separate the dashboard content
module Kibana
  def self.dashboard_content
    {
      "version": '6.6.1',
      "objects": [
        {
          "id": 'e7abc7d0-3ba8-11e9-8c37-cb7fed333dd2',
          "type": 'visualization',
          "updated_at": '2019-09-10T16:46:49.472Z',
          "version": 1,
          "attributes": {
            "description": '',
            "kibanaSavedObjectMeta": {
              "searchSourceJSON": "{\"index\":\"#{@index}\",\"query\":{\"query\":\"source: \\\"production_json.log\\\" NOT path: \\\"metrics\\\"\",\"language\":\"lucene\"},\"filter\":[]}"
            },
            "title": 'Top 25 Used Paths',
            "uiStateJSON": '{}',
            "version": 1,
            "visState": '{"title":"Top 25 Used Paths","type":"horizontal_bar","params":{"type":"histogram","grid":{"categoryLines":false,"style":{"color":"#eee"},"valueAxis":null},"categoryAxes":[{"id":"CategoryAxis-1","type":"category","position":"left","show":true,"style":{},"scale":{"type":"linear"},"labels":{"show":true,"rotate":0,"filter":false,"truncate":200},"title":{}}],"valueAxes":[{"id":"ValueAxis-1","name":"LeftAxis-1","type":"value","position":"bottom","show":true,"style":{},"scale":{"type":"linear","mode":"normal"},"labels":{"show":true,"rotate":75,"filter":true,"truncate":100},"title":{"text":"Count"}}],"seriesParams":[{"show":true,"type":"histogram","mode":"normal","data":{"label":"Count","id":"1"},"valueAxis":"ValueAxis-1","drawLinesBetweenPoints":true,"showCircles":true}],"addTooltip":true,"addLegend":true,"legendPosition":"top","times":[],"addTimeMarker":false,"orderBucketsBySum":false},"aggs":[{"id":"1","enabled":true,"type":"count","schema":"metric","params":{"customLabel":"Count"}},{"id":"2","enabled":true,"type":"terms","schema":"segment","params":{"field":"path.keyword","size":25,"order":"desc","orderBy":"1","otherBucket":false,"otherBucketLabel":"Other","missingBucket":false,"missingBucketLabel":"Missing","customLabel":"Path"}}]}'
          }
        },
        {
          "id": 'e4aa47a0-3ba8-11e9-8c37-cb7fed333dd2',
          "type": 'visualization',
          "updated_at": '2019-09-10T16:46:49.472Z',
          "version": 1,
          "attributes": {
            "description": '',
            "kibanaSavedObjectMeta": {
              "searchSourceJSON": "{\"index\":\"#{@index}\",\"filter\":[],\"query\":{\"query\":\"source: \\\"gitaly/current\\\"\",\"language\":\"lucene\"}}"
            },
            "title": 'Gitaly Duration Top Projects',
            "uiStateJSON": '{}',
            "version": 1,
            "visState": '{"title":"Gitaly Duration Top Projects","type":"horizontal_bar","params":{"type":"histogram","grid":{"categoryLines":false,"style":{"color":"#eee"}},"categoryAxes":[{"id":"CategoryAxis-1","type":"category","position":"left","show":true,"style":{},"scale":{"type":"linear"},"labels":{"show":true,"rotate":0,"filter":false,"truncate":200},"title":{}}],"valueAxes":[{"id":"ValueAxis-1","name":"LeftAxis-1","type":"value","position":"bottom","show":true,"style":{},"scale":{"type":"linear","mode":"normal"},"labels":{"show":true,"rotate":75,"filter":true,"truncate":100},"title":{"text":"Average"}}],"seriesParams":[{"show":true,"type":"histogram","mode":"normal","data":{"label":"Average","id":"1"},"valueAxis":"ValueAxis-1","drawLinesBetweenPoints":true,"showCircles":true}],"addTooltip":true,"addLegend":true,"legendPosition":"top","times":[],"addTimeMarker":false},"aggs":[{"id":"1","enabled":true,"type":"avg","schema":"metric","params":{"field":"grpc.time_ms","customLabel":"Average"}},{"id":"2","enabled":true,"type":"terms","schema":"segment","params":{"field":"grpc.request.repoPath.keyword","size":5,"order":"desc","orderBy":"1","otherBucket":false,"otherBucketLabel":"Other","missingBucket":false,"missingBucketLabel":"Missing","customLabel":"Repo"}}]}'
          }
        },
        {
          "id": 'e6778d90-3ba8-11e9-8c37-cb7fed333dd2',
          "type": 'visualization',
          "updated_at": '2019-09-10T16:46:49.472Z',
          "version": 1,
          "attributes": {
            "description": '',
            "kibanaSavedObjectMeta": {
              "searchSourceJSON": "{\"index\":\"#{@index}\",\"filter\":[],\"query\":{\"query\":\"_exists_: duration AND source:\\\"production_json.log\\\"  AND duration: [250 TO *]\",\"language\":\"lucene\"}}"
            },
            "title": 'Top Paths by Duration >250ms',
            "uiStateJSON": '{}',
            "version": 1,
            "visState": '{"title":"Top Paths by Duration >250ms","type":"horizontal_bar","params":{"type":"histogram","grid":{"categoryLines":false,"style":{"color":"#eee"}},"categoryAxes":[{"id":"CategoryAxis-1","type":"category","position":"left","show":true,"style":{},"scale":{"type":"linear"},"labels":{"show":true,"rotate":0,"filter":false,"truncate":200},"title":{}}],"valueAxes":[{"id":"ValueAxis-1","name":"LeftAxis-1","type":"value","position":"bottom","show":true,"style":{},"scale":{"type":"linear","mode":"normal"},"labels":{"show":true,"rotate":75,"filter":true,"truncate":100},"title":{"text":"Duration"}}],"seriesParams":[{"show":true,"type":"histogram","mode":"normal","data":{"label":"Duration","id":"1"},"valueAxis":"ValueAxis-1","drawLinesBetweenPoints":true,"showCircles":true}],"addTooltip":true,"addLegend":true,"legendPosition":"top","times":[],"addTimeMarker":false},"aggs":[{"id":"1","enabled":true,"type":"avg","schema":"metric","params":{"field":"duration","customLabel":"Duration"}},{"id":"2","enabled":true,"type":"terms","schema":"segment","params":{"field":"path.keyword","size":25,"order":"desc","orderBy":"1","customLabel":"Path"}}]}'
          }
        },
        {
          "id": 'e5de5ad0-3ba8-11e9-8c37-cb7fed333dd2',
          "type": 'visualization',
          "updated_at": '2019-09-10T16:46:49.472Z',
          "version": 1,
          "attributes": {
            "description": '',
            "kibanaSavedObjectMeta": {
              "searchSourceJSON": "{\"index\":\"#{@index}\",\"filter\":[],\"query\":{\"query\":\"source: \\\"production_json.log\\\"\",\"language\":\"lucene\"}}"
            },
            "title": 'Top Paths by Duration',
            "uiStateJSON": '{}',
            "version": 1,
            "visState": '{"title":"Top Paths by Duration","type":"horizontal_bar","params":{"type":"histogram","grid":{"categoryLines":false,"style":{"color":"#eee"}},"categoryAxes":[{"id":"CategoryAxis-1","type":"category","position":"left","show":true,"style":{},"scale":{"type":"linear"},"labels":{"show":true,"rotate":0,"filter":false,"truncate":200},"title":{}}],"valueAxes":[{"id":"ValueAxis-1","name":"LeftAxis-1","type":"value","position":"bottom","show":true,"style":{},"scale":{"type":"linear","mode":"normal"},"labels":{"show":true,"rotate":75,"filter":true,"truncate":100},"title":{"text":"Duration"}}],"seriesParams":[{"show":true,"type":"histogram","mode":"normal","data":{"label":"Duration","id":"1"},"valueAxis":"ValueAxis-1","drawLinesBetweenPoints":true,"showCircles":true}],"addTooltip":true,"addLegend":true,"legendPosition":"top","times":[],"addTimeMarker":false},"aggs":[{"id":"1","enabled":true,"type":"avg","schema":"metric","params":{"field":"duration","customLabel":"Duration"}},{"id":"2","enabled":true,"type":"terms","schema":"segment","params":{"field":"path.keyword","size":25,"order":"desc","orderBy":"1","customLabel":"Path"}}]}'
          }
        },
        {
          "id": 'e405f150-3ba8-11e9-8c37-cb7fed333dd2',
          "type": 'visualization',
          "updated_at": '2019-09-10T16:46:49.472Z',
          "version": 1,
          "attributes": {
            "description": '',
            "kibanaSavedObjectMeta": {
              "searchSourceJSON": "{\"index\":\"#{@index}\",\"query\":{\"query\":\"source: \\\"gitaly/current\\\"\",\"language\":\"lucene\"},\"filter\":[]}"
            },
            "title": 'Gitaly Method Stats',
            "uiStateJSON": '{}',
            "version": 1,
            "visState": '{"title":"Gitaly Method Stats","type":"table","params":{"perPage":25,"showPartialRows":false,"showMetricsAtAllLevels":true,"sort":{"columnIndex":null,"direction":null},"showTotal":true,"totalFunc":"sum"},"aggs":[{"id":"1","enabled":true,"type":"count","schema":"metric","params":{}},{"id":"3","enabled":true,"type":"percentiles","schema":"metric","params":{"field":"grpc.time_ms","percents":[99,95,50],"customLabel":"Percentile"}},{"id":"4","enabled":true,"type":"std_dev","schema":"metric","params":{"field":"grpc.time_ms","customLabel":"Deviation"}},{"id":"6","enabled":true,"type":"max","schema":"metric","params":{"field":"grpc.time_ms","customLabel":"Max"}},{"id":"5","enabled":true,"type":"min","schema":"metric","params":{"field":"grpc.time_ms","customLabel":"Min"}},{"id":"2","enabled":true,"type":"terms","schema":"bucket","params":{"field":"grpc.method.keyword","size":25,"order":"desc","orderBy":"1","otherBucket":false,"otherBucketLabel":"Other","missingBucket":false,"missingBucketLabel":"Missing","customLabel":"Method"}}]}'
          }
        },
        {
          "id": 'e2c72fc0-3ba8-11e9-8c37-cb7fed333dd2',
          "type": 'visualization',
          "updated_at": '2019-09-10T16:46:49.472Z',
          "version": 1,
          "attributes": {
            "description": '',
            "kibanaSavedObjectMeta": {
              "searchSourceJSON": "{\"index\":\"#{@index}\",\"query\":{\"query\":\"\",\"language\":\"lucene\"},\"filter\":[]}"
            },
            "title": 'Controller Stats',
            "uiStateJSON": '{}',
            "version": 1,
            "visState": '{"title":"Controller Stats","type":"table","params":{"perPage":20,"showPartialRows":false,"showMetricsAtAllLevels":false,"sort":{"columnIndex":null,"direction":null},"showTotal":false,"totalFunc":"sum"},"aggs":[{"id":"1","enabled":true,"type":"count","schema":"metric","params":{}},{"id":"2","enabled":true,"type":"terms","schema":"bucket","params":{"field":"controller.keyword","size":25,"order":"desc","orderBy":"1","otherBucket":false,"otherBucketLabel":"Other","missingBucket":false,"missingBucketLabel":"Missing","customLabel":"Controller"}},{"id":"3","enabled":true,"type":"percentiles","schema":"metric","params":{"field":"duration","percents":[99,95,50],"customLabel":"Percentile"}},{"id":"4","enabled":true,"type":"std_dev","schema":"metric","params":{"field":"duration","customLabel":""}},{"id":"5","enabled":true,"type":"max","schema":"metric","params":{"field":"duration"}},{"id":"6","enabled":true,"type":"min","schema":"metric","params":{"field":"duration"}}]}'
          }
        },
        {
          "id": 'e8e9ed20-3ba8-11e9-8c37-cb7fed333dd2',
          "type": 'visualization',
          "updated_at": '2019-09-10T16:46:49.472Z',
          "version": 1,
          "attributes": {
            "title": 'Top Paths by 5xx',
            "visState": '{"title":"Top Paths by 5xx","type":"horizontal_bar","params":{"type":"histogram","grid":{"categoryLines":false,"style":{"color":"#eee"}},"categoryAxes":[{"id":"CategoryAxis-1","type":"category","position":"left","show":true,"style":{},"scale":{"type":"linear"},"labels":{"show":true,"rotate":0,"filter":false,"truncate":200},"title":{}}],"valueAxes":[{"id":"ValueAxis-1","name":"LeftAxis-1","type":"value","position":"bottom","show":true,"style":{},"scale":{"type":"linear","mode":"normal"},"labels":{"show":true,"rotate":75,"filter":true,"truncate":100},"title":{"text":"Count"}}],"seriesParams":[{"show":true,"type":"histogram","mode":"normal","data":{"label":"Count","id":"1"},"valueAxis":"ValueAxis-1","drawLinesBetweenPoints":true,"showCircles":true}],"addTooltip":true,"addLegend":true,"legendPosition":"top","times":[],"addTimeMarker":false},"aggs":[{"id":"1","enabled":true,"type":"count","schema":"metric","params":{"customLabel":"Count"}},{"id":"2","enabled":true,"type":"terms","schema":"segment","params":{"field":"path.keyword","size":10,"order":"desc","orderBy":"1","otherBucket":false,"otherBucketLabel":"Other","missingBucket":false,"missingBucketLabel":"Missing","customLabel":"Path"}},{"id":"4","enabled":true,"type":"terms","schema":"split","params":{"field":"status","size":5,"order":"desc","orderBy":"1","otherBucket":false,"otherBucketLabel":"Other","missingBucket":false,"missingBucketLabel":"Missing","customLabel":"Status","row":true}}]}',
            "uiStateJSON": '{}',
            "description": '',
            "version": 1,
            "kibanaSavedObjectMeta": {
              "searchSourceJSON": "{\"index\":\"#{@index}\",\"query\":{\"query\":\"source: \\\"production_json.log\\\" NOT path: \\\"metrics\\\" AND status: [500 TO *]\",\"language\":\"lucene\"},\"filter\":[]}"
            }
          }
        },
        {
          "id": 'e54416a0-3ba8-11e9-8c37-cb7fed333dd2',
          "type": 'visualization',
          "updated_at": '2019-09-10T16:46:49.472Z',
          "version": 1,
          "attributes": {
            "description": '',
            "kibanaSavedObjectMeta": {
              "searchSourceJSON": "{\"index\":\"#{@index}\",\"filter\":[],\"query\":{\"language\":\"lucene\",\"query\":\"_exists_: username AND source:\\\"production_json.log\\\"\"}}"
            },
            "title": 'Request Count Per User',
            "uiStateJSON": '{"vis":{"colors":{"Request Count":"#65C5DB"},"legendOpen":true}}',
            "version": 1,
            "visState": '{"title":"Request Count Per User","type":"horizontal_bar","params":{"type":"histogram","grid":{"categoryLines":false,"style":{"color":"#eee"}},"categoryAxes":[{"id":"CategoryAxis-1","type":"category","position":"left","show":true,"style":{},"scale":{"type":"linear"},"labels":{"show":true,"rotate":0,"filter":false,"truncate":200},"title":{}}],"valueAxes":[{"id":"ValueAxis-1","name":"LeftAxis-1","type":"value","position":"bottom","show":true,"style":{},"scale":{"type":"linear","mode":"normal"},"labels":{"show":true,"rotate":75,"filter":true,"truncate":100},"title":{"text":"Request Count"}}],"seriesParams":[{"show":true,"type":"histogram","mode":"normal","data":{"label":"Request Count","id":"1"},"valueAxis":"ValueAxis-1","drawLinesBetweenPoints":true,"showCircles":true}],"addTooltip":true,"addLegend":true,"legendPosition":"top","times":[],"addTimeMarker":false},"aggs":[{"id":"1","enabled":true,"type":"count","schema":"metric","params":{"customLabel":"Request Count"}},{"id":"2","enabled":true,"type":"terms","schema":"segment","params":{"field":"username.keyword","size":20,"order":"desc","orderBy":"1","customLabel":"Username"}}]}'
          }
        },
        {
          "id": 'c8c393c0-41d9-11e9-b300-9f3a55703eb8',
          "type": 'visualization',
          "updated_at": '2019-09-10T16:46:49.472Z',
          "version": 1,
          "attributes": {
            "title": 'Request Status Over Time',
            "visState": '{"title":"Request Status Over Time","type":"metrics","params":{"id":"61ca57f0-469d-11e7-af02-69e470af7417","type":"timeseries","series":[{"id":"61ca57f1-469d-11e7-af02-69e470af7417","color":"#68BC00","split_mode":"terms","metrics":[{"id":"61ca57f2-469d-11e7-af02-69e470af7417","type":"count","field":"duration"}],"separate_axis":0,"axis_position":"right","formatter":"number","chart_type":"line","line_width":1,"point_size":1,"fill":0.5,"stacked":"none","terms_field":"status","terms_order_by":"61ca57f2-469d-11e7-af02-69e470af7417","terms_size":"10","split_filters":[{"color":"#68BC00","id":"621fc2d0-41dc-11e9-b19f-f1f3501de2b0"}],"value_template":"{{count}}","hide_in_legend":0,"steps":0,"series_drop_last_bucket":1}],"time_field":"time","index_pattern":"logstash-*","interval":"auto","axis_position":"left","axis_formatter":"number","axis_scale":"normal","show_legend":1,"show_grid":1,"background_color_rules":[{"id":"113e2ea0-41d8-11e9-93ea-b907d6723b83"}],"bar_color_rules":[{"id":"40226c90-41d8-11e9-93ea-b907d6723b83"}],"gauge_color_rules":[{"id":"42bacab0-41d8-11e9-93ea-b907d6723b83"}],"gauge_width":10,"gauge_inner_width":10,"gauge_style":"half","filter":"source: \\"gitlab-rails/production_json.log\\" "},"aggs":[]}',
            "uiStateJSON": '{}',
            "description": '',
            "version": 1,
            "kibanaSavedObjectMeta": {
              "searchSourceJSON": '{"query":{"query":"","language":"lucene"},"filter":[]}'
            }
          }
        },
        {
          "id": '248fdf90-41dc-11e9-b300-9f3a55703eb8',
          "type": 'visualization',
          "updated_at": '2019-09-10T16:46:49.472Z',
          "version": 1,
          "attributes": {
            "title": 'Request Duration / Count',
            "visState": '{"title":"Request Duration / Count","type":"histogram","params":{"addLegend":true,"addTimeMarker":false,"addTooltip":true,"categoryAxes":[{"id":"CategoryAxis-1","labels":{"show":true,"truncate":100},"position":"bottom","scale":{"type":"linear"},"show":true,"style":{},"title":{},"type":"category"}],"grid":{"categoryLines":false,"style":{"color":"#eee"}},"legendPosition":"right","seriesParams":[{"data":{"id":"1","label":"Average duration"},"drawLinesBetweenPoints":true,"mode":"normal","show":"true","showCircles":true,"type":"area","valueAxis":"ValueAxis-1","interpolate":"linear"},{"data":{"id":"3","label":"Count"},"drawLinesBetweenPoints":true,"mode":"stacked","show":true,"showCircles":true,"type":"area","valueAxis":"ValueAxis-1"}],"times":[],"type":"histogram","valueAxes":[{"id":"ValueAxis-1","labels":{"filter":false,"rotate":0,"show":true,"truncate":100},"name":"LeftAxis-1","position":"left","scale":{"mode":"normal","type":"linear"},"show":true,"style":{},"title":{"text":"Average duration"},"type":"value"}]},"aggs":[{"id":"1","enabled":true,"type":"avg","schema":"metric","params":{"field":"duration"}},{"id":"2","enabled":true,"type":"date_histogram","schema":"segment","params":{"field":"time","timeRange":{"from":"now-30d","to":"now","mode":"quick"},"useNormalizedEsInterval":true,"interval":"auto","time_zone":"America/Denver","drop_partials":false,"customInterval":"2h","min_doc_count":1,"extended_bounds":{}}},{"id":"3","enabled":true,"type":"count","schema":"metric","params":{}},{"id":"4","enabled":true,"type":"terms","schema":"split","params":{"field":"host.keyword","size":5,"order":"desc","orderBy":"1","otherBucket":false,"otherBucketLabel":"Other","missingBucket":false,"missingBucketLabel":"Missing","row":true}}]}',
            "uiStateJSON": '{"vis":{"colors":{"Average duration":"#C15C17","Count":"#70DBED"}}}',
            "description": '',
            "version": 1,
            "kibanaSavedObjectMeta": {
              "searchSourceJSON": "{\"index\":\"#{@index}\",\"query\":{\"language\":\"lucene\",\"query\":\"source: \\\"production_json.log\\\"\"},\"filter\":[]}"
            }
          }
        },
        {
          "id": '96e165d0-41de-11e9-b300-9f3a55703eb8',
          "type": 'visualization',
          "updated_at": '2019-09-10T16:46:49.472Z',
          "version": 1,
          "attributes": {
            "title": 'Request Over Time by Status',
            "visState": '{"title":"Request Over Time by Status","type":"area","params":{"type":"area","grid":{"categoryLines":true,"style":{"color":"#eee"}},"categoryAxes":[{"id":"CategoryAxis-1","type":"category","position":"bottom","show":true,"style":{},"scale":{"type":"linear"},"labels":{"show":true,"truncate":100},"title":{}}],"valueAxes":[{"id":"ValueAxis-1","name":"LeftAxis-1","type":"value","position":"left","show":true,"style":{},"scale":{"type":"linear","mode":"normal"},"labels":{"show":true,"rotate":0,"filter":false,"truncate":100},"title":{"text":"Count"}}],"seriesParams":[{"show":"true","type":"area","mode":"normal","data":{"label":"Count","id":"1"},"drawLinesBetweenPoints":true,"showCircles":true,"interpolate":"linear","valueAxis":"ValueAxis-1"}],"addTooltip":true,"addLegend":true,"legendPosition":"right","times":[],"addTimeMarker":false},"aggs":[{"id":"1","enabled":true,"type":"count","schema":"metric","params":{}},{"id":"2","enabled":true,"type":"date_histogram","schema":"segment","params":{"field":"time","timeRange":{"from":"now-30d","to":"now","mode":"quick"},"useNormalizedEsInterval":true,"interval":"auto","time_zone":"America/Denver","drop_partials":false,"customInterval":"2h","min_doc_count":1,"extended_bounds":{}}},{"id":"3","enabled":true,"type":"terms","schema":"group","params":{"field":"status","size":5,"order":"desc","orderBy":"1","otherBucket":false,"otherBucketLabel":"Other","missingBucket":false,"missingBucketLabel":"Missing"}},{"id":"4","enabled":true,"type":"terms","schema":"split","params":{"field":"host.keyword","size":5,"order":"desc","orderBy":"1","otherBucket":false,"otherBucketLabel":"Other","missingBucket":false,"missingBucketLabel":"Missing","row":true}}]}',
            "uiStateJSON": '{}',
            "description": '',
            "version": 1,
            "kibanaSavedObjectMeta": {
              "searchSourceJSON": "{\"index\":\"#{@index}\",\"query\":{\"query\":\"source: \\\"production_json.log\\\" AND NOT status: 200\",\"language\":\"lucene\"},\"filter\":[]}"
            }
          }
        },
        {
          "id": 'cf7c3ba0-41f1-11e9-b300-9f3a55703eb8',
          "type": 'visualization',
          "updated_at": '2019-09-10T16:46:49.472Z',
          "version": 1,
          "attributes": {
            "title": 'Gitaly Duration Over Time',
            "visState": '{"title":"Gitaly Duration Over Time","type":"line","params":{"type":"line","grid":{"categoryLines":false,"style":{"color":"#eee"}},"categoryAxes":[{"id":"CategoryAxis-1","type":"category","position":"bottom","show":true,"style":{},"scale":{"type":"linear"},"labels":{"show":true,"truncate":100},"title":{}}],"valueAxes":[{"id":"ValueAxis-1","name":"LeftAxis-1","type":"value","position":"left","show":true,"style":{},"scale":{"type":"linear","mode":"normal"},"labels":{"show":true,"rotate":0,"filter":false,"truncate":100},"title":{"text":"Duration"}}],"seriesParams":[{"show":"true","type":"area","mode":"normal","data":{"label":"Duration","id":"1"},"valueAxis":"ValueAxis-1","drawLinesBetweenPoints":true,"showCircles":true}],"addTooltip":true,"addLegend":true,"legendPosition":"right","times":[],"addTimeMarker":false},"aggs":[{"id":"1","enabled":true,"type":"avg","schema":"metric","params":{"field":"grpc.time_ms","customLabel":"Duration"}},{"id":"2","enabled":true,"type":"date_histogram","schema":"segment","params":{"field":"time","timeRange":{"from":"now-30d","to":"now","mode":"quick"},"useNormalizedEsInterval":true,"interval":"auto","time_zone":"America/Denver","drop_partials":false,"customInterval":"2h","min_doc_count":1,"extended_bounds":{}}},{"id":"3","enabled":true,"type":"terms","schema":"split","params":{"field":"host.keyword","size":5,"order":"desc","orderBy":"1","otherBucket":false,"otherBucketLabel":"Other","missingBucket":false,"missingBucketLabel":"Missing","row":true}}]}',
            "uiStateJSON": '{"vis":{"colors":{"Duration":"#F9934E"}}}',
            "description": '',
            "version": 1,
            "kibanaSavedObjectMeta": {
              "searchSourceJSON": "{\"index\":\"#{@index}\",\"query\":{\"query\":\"source: \\\"gitaly/current\\\"\",\"language\":\"lucene\"},\"filter\":[]}"
            }
          }
        },
        {
          "id": '4f400dc0-41fd-11e9-b31f-bdfff4e56f4d',
          "type": 'visualization',
          "updated_at": '2019-09-10T16:46:49.472Z',
          "version": 1,
          "attributes": {
            "title": 'Gitaly Method Over Time',
            "visState": '{"title":"Gitaly Method Over Time","type":"area","params":{"type":"area","grid":{"categoryLines":false,"style":{"color":"#eee"}},"categoryAxes":[{"id":"CategoryAxis-1","type":"category","position":"bottom","show":true,"style":{},"scale":{"type":"linear"},"labels":{"show":true,"truncate":100},"title":{}}],"valueAxes":[{"id":"ValueAxis-1","name":"LeftAxis-1","type":"value","position":"left","show":true,"style":{},"scale":{"type":"linear","mode":"normal"},"labels":{"show":true,"rotate":0,"filter":false,"truncate":100},"title":{"text":"Count"}}],"seriesParams":[{"show":"true","type":"area","mode":"normal","data":{"label":"Count","id":"1"},"drawLinesBetweenPoints":true,"showCircles":true,"interpolate":"linear","valueAxis":"ValueAxis-1"}],"addTooltip":true,"addLegend":true,"legendPosition":"right","times":[],"addTimeMarker":false},"aggs":[{"id":"1","enabled":true,"type":"count","schema":"metric","params":{}},{"id":"2","enabled":true,"type":"date_histogram","schema":"segment","params":{"field":"time","timeRange":{"from":"now-30d","to":"now","mode":"quick"},"useNormalizedEsInterval":true,"interval":"auto","time_zone":"America/Denver","drop_partials":false,"customInterval":"2h","min_doc_count":1,"extended_bounds":{}}},{"id":"3","enabled":true,"type":"terms","schema":"group","params":{"field":"grpc.method.keyword","size":5,"order":"desc","orderBy":"1","otherBucket":false,"otherBucketLabel":"Other","missingBucket":false,"missingBucketLabel":"Missing"}},{"id":"4","enabled":true,"type":"terms","schema":"split","params":{"field":"host.keyword","size":5,"order":"desc","orderBy":"1","otherBucket":false,"otherBucketLabel":"Other","missingBucket":false,"missingBucketLabel":"Missing","row":true}}]}',
            "uiStateJSON": '{}',
            "description": '',
            "version": 1,
            "kibanaSavedObjectMeta": {
              "searchSourceJSON": "{\"index\":\"#{@index}\",\"query\":{\"query\":\"source: \\\"gitaly/current\\\"\",\"language\":\"lucene\"},\"filter\":[]}"
            }
          }
        },
        {
          "id": '6072ff80-c439-11e9-9d80-2b20017a3c16',
          "type": 'visualization',
          "updated_at": '2019-09-10T16:52:23.198Z',
          "version": 2,
          "attributes": {
            "title": 'Log Sources',
            "visState": '{"title":"Log Sources","type":"table","params":{"perPage":10,"showPartialRows":false,"showMetricsAtAllLevels":false,"sort":{"columnIndex":null,"direction":null},"showTotal":false,"totalFunc":"sum"},"aggs":[{"id":"1","enabled":true,"type":"count","schema":"metric","params":{}},{"id":"2","enabled":true,"type":"terms","schema":"bucket","params":{"field":"source.keyword","size":25,"order":"desc","orderBy":"1","otherBucket":false,"otherBucketLabel":"Other","missingBucket":false,"missingBucketLabel":"Missing","customLabel":"Files"}}]}',
            "uiStateJSON": '{"vis":{"params":{"sort":{"columnIndex":null,"direction":null}}}}',
            "description": '',
            "version": 1,
            "kibanaSavedObjectMeta": {
              "searchSourceJSON": "{\"index\":\"#{@index}\",\"query\":{\"query\":\"\",\"language\":\"lucene\"},\"filter\":[]}"
            }
          }
        },
        {
          "id": 'e21e1b70-d3eb-11e9-9313-01e5c8765727',
          "type": 'visualization',
          "updated_at": '2019-09-10T16:56:07.591Z',
          "version": 1,
          "attributes": {
            "title": 'Registry Request Count',
            "visState": '{"title":"Registry Request Count","type":"histogram","params":{"type":"histogram","grid":{"categoryLines":false,"style":{"color":"#eee"}},"categoryAxes":[{"id":"CategoryAxis-1","type":"category","position":"bottom","show":true,"style":{},"scale":{"type":"linear"},"labels":{"show":true,"truncate":100,"rotate":0},"title":{}}],"valueAxes":[{"id":"ValueAxis-1","name":"LeftAxis-1","type":"value","position":"left","show":true,"style":{},"scale":{"type":"linear","mode":"normal"},"labels":{"show":true,"rotate":0,"filter":false,"truncate":100},"title":{"text":"Count"}}],"seriesParams":[{"show":"true","type":"histogram","mode":"stacked","data":{"label":"Count","id":"1"},"valueAxis":"ValueAxis-1","drawLinesBetweenPoints":true,"showCircles":true}],"addTooltip":true,"addLegend":true,"legendPosition":"right","times":[],"addTimeMarker":false},"aggs":[{"id":"1","enabled":true,"type":"count","schema":"metric","params":{}},{"id":"2","enabled":true,"type":"terms","schema":"segment","params":{"field":"http.request.uri.keyword","size":15,"order":"desc","orderBy":"1","otherBucket":false,"otherBucketLabel":"Other","missingBucket":false,"missingBucketLabel":"Missing","customLabel":"Endpoint"}}]}',
            "uiStateJSON": '{}',
            "description": '',
            "version": 1,
            "kibanaSavedObjectMeta": {
              "searchSourceJSON": "{\"index\":\"#{@index}\",\"query\":{\"query\":\"\",\"language\":\"lucene\"},\"filter\":[{\"meta\":{\"index\":\"#{@index}\",\"negate\":false,\"disabled\":false,\"alias\":null,\"type\":\"phrase\",\"key\":\"source\",\"value\":\"registry/current\",\"params\":{\"query\":\"registry/current\",\"type\":\"phrase\"}},\"query\":{\"match\":{\"source\":{\"query\":\"registry/current\",\"type\":\"phrase\"}}},\"$state\":{\"store\":\"appState\"}}]}"
            }
          }
        },
        {
          "id": '5b891090-d3ed-11e9-9313-01e5c8765727',
          "type": 'visualization',
          "updated_at": '2019-09-10T17:13:12.151Z',
          "version": 2,
          "attributes": {
            "title": 'API Duration/Count',
            "visState": '{"title":"API Duration/Count","type":"area","params":{"type":"area","grid":{"categoryLines":false,"style":{"color":"#eee"}},"categoryAxes":[{"id":"CategoryAxis-1","type":"category","position":"bottom","show":true,"style":{},"scale":{"type":"linear"},"labels":{"show":true,"truncate":100},"title":{}}],"valueAxes":[{"id":"ValueAxis-1","name":"LeftAxis-1","type":"value","position":"left","show":true,"style":{},"scale":{"type":"linear","mode":"normal"},"labels":{"show":true,"rotate":0,"filter":false,"truncate":100},"title":{"text":"Average duration"}}],"seriesParams":[{"show":"true","type":"area","mode":"normal","data":{"label":"Average duration","id":"1"},"drawLinesBetweenPoints":true,"showCircles":true,"interpolate":"linear","valueAxis":"ValueAxis-1"},{"show":true,"mode":"stacked","type":"area","drawLinesBetweenPoints":true,"showCircles":true,"interpolate":"linear","data":{"id":"3","label":"Count"},"valueAxis":"ValueAxis-1"}],"addTooltip":true,"addLegend":true,"legendPosition":"right","times":[],"addTimeMarker":false},"aggs":[{"id":"1","enabled":true,"type":"avg","schema":"metric","params":{"field":"duration"}},{"id":"2","enabled":true,"type":"date_histogram","schema":"segment","params":{"field":"time","timeRange":{"from":"2019-09-09T15:35:37.114Z","to":"2019-09-10T15:32:21.639Z","mode":"absolute"},"useNormalizedEsInterval":true,"interval":"auto","time_zone":"America/Denver","drop_partials":false,"customInterval":"2h","min_doc_count":1,"extended_bounds":{}}},{"id":"3","enabled":true,"type":"count","schema":"metric","params":{}}]}',
            "uiStateJSON": '{"vis":{"legendOpen":false,"colors":{"Average duration":"#F4D598","Count":"#5195CE"}}}',
            "description": '',
            "version": 1,
            "kibanaSavedObjectMeta": {
              "searchSourceJSON": "{\"index\":\"#{@index}\",\"query\":{\"query\":\"\",\"language\":\"lucene\"},\"filter\":[{\"meta\":{\"index\":\"#{@index}\",\"negate\":false,\"disabled\":false,\"alias\":null,\"type\":\"phrase\",\"key\":\"source\",\"value\":\"gitlab-rails/api_json.log\",\"params\":{\"query\":\"gitlab-rails/api_json.log\",\"type\":\"phrase\"}},\"query\":{\"match\":{\"source\":{\"query\":\"gitlab-rails/api_json.log\",\"type\":\"phrase\"}}},\"$state\":{\"store\":\"appState\"}}]}"
            }
          }
        },
        {
          "id": 'b8811d10-d3ed-11e9-9313-01e5c8765727',
          "type": 'visualization',
          "updated_at": '2019-09-10T17:09:16.769Z',
          "version": 1,
          "attributes": {
            "title": 'API Duration/Path',
            "visState": '{"title":"API Duration/Path","type":"area","params":{"type":"area","grid":{"categoryLines":false,"style":{"color":"#eee"}},"categoryAxes":[{"id":"CategoryAxis-1","type":"category","position":"bottom","show":true,"style":{},"scale":{"type":"linear"},"labels":{"show":true,"truncate":100,"rotate":0},"title":{}}],"valueAxes":[{"id":"ValueAxis-1","name":"LeftAxis-1","type":"value","position":"left","show":true,"style":{},"scale":{"type":"linear","mode":"normal"},"labels":{"show":true,"rotate":0,"filter":false,"truncate":100},"title":{"text":"Average duration"}}],"seriesParams":[{"show":"true","type":"area","mode":"normal","data":{"label":"Average duration","id":"1"},"drawLinesBetweenPoints":true,"showCircles":true,"interpolate":"linear","valueAxis":"ValueAxis-1"}],"addTooltip":true,"addLegend":true,"legendPosition":"right","times":[],"addTimeMarker":false},"aggs":[{"id":"1","enabled":true,"type":"avg","schema":"metric","params":{"field":"duration"}},{"id":"2","enabled":true,"type":"date_histogram","schema":"segment","params":{"field":"time","timeRange":{"from":"2019-09-09T15:35:37.114Z","to":"2019-09-10T15:32:21.639Z","mode":"absolute"},"useNormalizedEsInterval":true,"interval":"auto","time_zone":"America/Denver","drop_partials":false,"customInterval":"2h","min_doc_count":1,"extended_bounds":{}}},{"id":"3","enabled":true,"type":"terms","schema":"group","params":{"field":"path.keyword","size":10,"order":"desc","orderBy":"1","otherBucket":true,"otherBucketLabel":"Other","missingBucket":false,"missingBucketLabel":"Missing","customLabel":"Path"}}]}',
            "uiStateJSON": '{}',
            "description": '',
            "version": 1,
            "kibanaSavedObjectMeta": {
              "searchSourceJSON": "{\"index\":\"#{@index}\",\"query\":{\"query\":\"\",\"language\":\"lucene\"},\"filter\":[{\"meta\":{\"index\":\"#{@index}\",\"negate\":false,\"disabled\":false,\"alias\":null,\"type\":\"phrase\",\"key\":\"source\",\"value\":\"gitlab-rails/api_json.log\",\"params\":{\"query\":\"gitlab-rails/api_json.log\",\"type\":\"phrase\"}},\"query\":{\"match\":{\"source\":{\"query\":\"gitlab-rails/api_json.log\",\"type\":\"phrase\"}}},\"$state\":{\"store\":\"appState\"}}]}"
            }
          }
        },
        {
          "id": '1bfbe140-d3ee-11e9-9313-01e5c8765727',
          "type": 'visualization',
          "updated_at": '2019-09-10T17:12:03.668Z',
          "version": 1,
          "attributes": {
            "title": 'API Top Paths Duration',
            "visState": '{"title":"API Top Paths Duration","type":"horizontal_bar","params":{"type":"histogram","grid":{"categoryLines":false,"style":{"color":"#eee"}},"categoryAxes":[{"id":"CategoryAxis-1","type":"category","position":"left","show":true,"style":{},"scale":{"type":"linear"},"labels":{"show":true,"rotate":0,"filter":false,"truncate":200},"title":{}}],"valueAxes":[{"id":"ValueAxis-1","name":"LeftAxis-1","type":"value","position":"bottom","show":true,"style":{},"scale":{"type":"linear","mode":"normal"},"labels":{"show":true,"rotate":75,"filter":true,"truncate":100},"title":{"text":"Average duration"}}],"seriesParams":[{"show":true,"type":"histogram","mode":"normal","data":{"label":"Average duration","id":"1"},"valueAxis":"ValueAxis-1","drawLinesBetweenPoints":true,"showCircles":true},{"show":true,"mode":"normal","type":"histogram","drawLinesBetweenPoints":true,"showCircles":true,"data":{"id":"3","label":"Count"},"valueAxis":"ValueAxis-1"}],"addTooltip":true,"addLegend":true,"legendPosition":"right","times":[],"addTimeMarker":false},"aggs":[{"id":"1","enabled":true,"type":"avg","schema":"metric","params":{"field":"duration"}},{"id":"2","enabled":true,"type":"terms","schema":"group","params":{"field":"path.keyword","size":5,"order":"desc","orderBy":"1","otherBucket":true,"otherBucketLabel":"Other","missingBucket":false,"missingBucketLabel":"Missing","customLabel":"Path"}},{"id":"3","enabled":false,"type":"count","schema":"metric","params":{}}]}',
            "uiStateJSON": '{}',
            "description": '',
            "version": 1,
            "kibanaSavedObjectMeta": {
              "searchSourceJSON": "{\"index\":\"#{@index}\",\"query\":{\"query\":\"\",\"language\":\"lucene\"},\"filter\":[{\"meta\":{\"index\":\"#{@index}\",\"negate\":false,\"disabled\":false,\"alias\":null,\"type\":\"phrase\",\"key\":\"source\",\"value\":\"gitlab-rails/api_json.log\",\"params\":{\"query\":\"gitlab-rails/api_json.log\",\"type\":\"phrase\"}},\"query\":{\"match\":{\"source\":{\"query\":\"gitlab-rails/api_json.log\",\"type\":\"phrase\"}}},\"$state\":{\"store\":\"appState\"}}]}"
            }
          }
        },
        {
          "id": 'c788e190-d3ec-11e9-9313-01e5c8765727',
          "type": 'visualization',
          "updated_at": '2019-09-10T17:02:32.489Z',
          "version": 1,
          "attributes": {
            "title": 'User API Count',
            "visState": '{"title":"User API Count","type":"horizontal_bar","params":{"type":"histogram","grid":{"categoryLines":false,"style":{"color":"#eee"}},"categoryAxes":[{"id":"CategoryAxis-1","type":"category","position":"left","show":true,"style":{},"scale":{"type":"linear"},"labels":{"show":true,"rotate":0,"filter":false,"truncate":200},"title":{}}],"valueAxes":[{"id":"ValueAxis-1","name":"LeftAxis-1","type":"value","position":"bottom","show":true,"style":{},"scale":{"type":"linear","mode":"normal"},"labels":{"show":true,"rotate":75,"filter":true,"truncate":100},"title":{"text":"Count"}}],"seriesParams":[{"show":true,"type":"histogram","mode":"normal","data":{"label":"Count","id":"1"},"valueAxis":"ValueAxis-1","drawLinesBetweenPoints":true,"showCircles":true}],"addTooltip":true,"addLegend":true,"legendPosition":"right","times":[],"addTimeMarker":false},"aggs":[{"id":"1","enabled":true,"type":"count","schema":"metric","params":{}},{"id":"2","enabled":true,"type":"terms","schema":"segment","params":{"field":"username.keyword","size":10,"order":"desc","orderBy":"1","otherBucket":false,"otherBucketLabel":"Other","missingBucket":false,"missingBucketLabel":"Missing","customLabel":"Username"}}]}',
            "uiStateJSON": '{}',
            "description": '',
            "version": 1,
            "kibanaSavedObjectMeta": {
              "searchSourceJSON": "{\"index\":\"#{@index}\",\"query\":{\"query\":\"_exists_: \\\"username\\\"\",\"language\":\"lucene\"},\"filter\":[{\"meta\":{\"index\":\"#{@index}\",\"negate\":false,\"disabled\":false,\"alias\":null,\"type\":\"phrase\",\"key\":\"source\",\"value\":\"gitlab-rails/api_json.log\",\"params\":{\"query\":\"gitlab-rails/api_json.log\",\"type\":\"phrase\"}},\"query\":{\"match\":{\"source\":{\"query\":\"gitlab-rails/api_json.log\",\"type\":\"phrase\"}}},\"$state\":{\"store\":\"appState\"}}]}"
            }
          }
        },
        {
          "id": 'd51e0aa0-d3e8-11e9-8369-c5395322e794',
          "type": 'visualization',
          "updated_at": '2019-09-10T16:46:49.472Z',
          "version": 1,
          "attributes": {
            "title": 'Sidekiq Class/Job Count',
            "visState": '{"title":"Sidekiq Class/Job Count","type":"horizontal_bar","params":{"type":"histogram","grid":{"categoryLines":true,"style":{"color":"#eee"}},"categoryAxes":[{"id":"CategoryAxis-1","type":"category","position":"left","show":true,"style":{},"scale":{"type":"linear"},"labels":{"show":true,"rotate":0,"filter":false,"truncate":200},"title":{}}],"valueAxes":[{"id":"ValueAxis-1","name":"LeftAxis-1","type":"value","position":"bottom","show":true,"style":{},"scale":{"type":"linear","mode":"normal"},"labels":{"show":true,"rotate":75,"filter":true,"truncate":100},"title":{"text":"Count"}}],"seriesParams":[{"show":true,"type":"histogram","mode":"normal","data":{"label":"Count","id":"1"},"valueAxis":"ValueAxis-1","drawLinesBetweenPoints":true,"showCircles":true}],"addTooltip":true,"addLegend":true,"legendPosition":"right","times":[],"addTimeMarker":false},"aggs":[{"id":"1","enabled":true,"type":"count","schema":"metric","params":{}},{"id":"2","enabled":true,"type":"terms","schema":"split","params":{"field":"class.keyword","size":5,"order":"desc","orderBy":"1","otherBucket":false,"otherBucketLabel":"Other","missingBucket":false,"missingBucketLabel":"Missing","row":true}}]}',
            "uiStateJSON": '{}',
            "description": '',
            "version": 1,
            "kibanaSavedObjectMeta": {
              "searchSourceJSON": "{\"index\":\"#{@index}\",\"query\":{\"query\":\"\",\"language\":\"lucene\"},\"filter\":[{\"meta\":{\"index\":\"#{@index}\",\"negate\":false,\"disabled\":false,\"alias\":null,\"type\":\"phrase\",\"key\":\"source\",\"value\":\"gitlab-rails/sidekiq.log\",\"params\":{\"query\":\"gitlab-rails/sidekiq.log\",\"type\":\"phrase\"}},\"query\":{\"match\":{\"source\":{\"query\":\"gitlab-rails/sidekiq.log\",\"type\":\"phrase\"}}},\"$state\":{\"store\":\"appState\"}}]}"
            }
          }
        },
        {
          "id": '90c52270-d3e9-11e9-8369-c5395322e794',
          "type": 'visualization',
          "updated_at": '2019-09-10T16:46:49.472Z',
          "version": 1,
          "attributes": {
            "title": 'Sidekiq Job Avg Duration',
            "visState": '{"title":"Sidekiq Job Avg Duration","type":"histogram","params":{"type":"histogram","grid":{"categoryLines":false,"style":{"color":"#eee"}},"categoryAxes":[{"id":"CategoryAxis-1","type":"category","position":"bottom","show":true,"style":{},"scale":{"type":"linear"},"labels":{"show":true,"truncate":100,"rotate":0},"title":{}}],"valueAxes":[{"id":"ValueAxis-1","name":"LeftAxis-1","type":"value","position":"left","show":true,"style":{},"scale":{"type":"linear","mode":"normal"},"labels":{"show":true,"rotate":0,"filter":false,"truncate":100},"title":{"text":"Average duration"}}],"seriesParams":[{"show":"true","type":"histogram","mode":"normal","data":{"label":"Average duration","id":"1"},"valueAxis":"ValueAxis-1","drawLinesBetweenPoints":true,"showCircles":true}],"addTooltip":true,"addLegend":true,"legendPosition":"right","times":[],"addTimeMarker":false},"aggs":[{"id":"1","enabled":true,"type":"avg","schema":"metric","params":{"field":"duration"}},{"id":"2","enabled":true,"type":"terms","schema":"segment","params":{"field":"class.keyword","size":10,"order":"desc","orderBy":"1","otherBucket":false,"otherBucketLabel":"Other","missingBucket":false,"missingBucketLabel":"Missing","customLabel":"Job"}}]}',
            "uiStateJSON": '{}',
            "description": '',
            "version": 1,
            "kibanaSavedObjectMeta": {
              "searchSourceJSON": "{\"index\":\"#{@index}\",\"query\":{\"query\":\"\",\"language\":\"lucene\"},\"filter\":[{\"meta\":{\"index\":\"#{@index}\",\"negate\":false,\"disabled\":false,\"alias\":null,\"type\":\"phrase\",\"key\":\"source\",\"value\":\"gitlab-rails/sidekiq.log\",\"params\":{\"query\":\"gitlab-rails/sidekiq.log\",\"type\":\"phrase\"}},\"query\":{\"match\":{\"source\":{\"query\":\"gitlab-rails/sidekiq.log\",\"type\":\"phrase\"}}},\"$state\":{\"store\":\"appState\"}}]}"
            }
          }
        },
        {
          "id": 'e2db14c0-d3e9-11e9-8369-c5395322e794',
          "type": 'visualization',
          "updated_at": '2019-09-10T16:46:49.472Z',
          "version": 1,
          "attributes": {
            "title": 'Sidekiq Job Avg Duration Over Time',
            "visState": '{"title":"Sidekiq Job Avg Duration Over Time","type":"line","params":{"type":"line","grid":{"categoryLines":false,"style":{"color":"#eee"}},"categoryAxes":[{"id":"CategoryAxis-1","type":"category","position":"bottom","show":true,"style":{},"scale":{"type":"linear"},"labels":{"show":true,"truncate":100},"title":{}}],"valueAxes":[{"id":"ValueAxis-1","name":"LeftAxis-1","type":"value","position":"left","show":true,"style":{},"scale":{"type":"linear","mode":"normal"},"labels":{"show":true,"rotate":0,"filter":false,"truncate":100},"title":{"text":"Average duration"}}],"seriesParams":[{"show":"true","type":"area","mode":"stacked","data":{"label":"Average duration","id":"1"},"valueAxis":"ValueAxis-1","drawLinesBetweenPoints":true,"showCircles":true,"interpolate":"step-after"}],"addTooltip":true,"addLegend":true,"legendPosition":"right","times":[],"addTimeMarker":false},"aggs":[{"id":"1","enabled":true,"type":"avg","schema":"metric","params":{"field":"duration"}},{"id":"2","enabled":true,"type":"date_histogram","schema":"segment","params":{"field":"time","timeRange":{"from":"now/d","to":"now/d","mode":"quick"},"useNormalizedEsInterval":true,"interval":"auto","time_zone":"America/Denver","drop_partials":false,"customInterval":"2h","min_doc_count":1,"extended_bounds":{}}},{"id":"3","enabled":true,"type":"terms","schema":"group","params":{"field":"class.keyword","size":5,"order":"desc","orderBy":"1","otherBucket":false,"otherBucketLabel":"Other","missingBucket":false,"missingBucketLabel":"Missing"}}]}',
            "uiStateJSON": '{}',
            "description": '',
            "version": 1,
            "kibanaSavedObjectMeta": {
              "searchSourceJSON": "{\"index\":\"#{@index}\",\"query\":{\"query\":\"\",\"language\":\"lucene\"},\"filter\":[{\"meta\":{\"index\":\"#{@index}\",\"negate\":false,\"disabled\":false,\"alias\":null,\"type\":\"phrase\",\"key\":\"source\",\"value\":\"gitlab-rails/sidekiq.log\",\"params\":{\"query\":\"gitlab-rails/sidekiq.log\",\"type\":\"phrase\"}},\"query\":{\"match\":{\"source\":{\"query\":\"gitlab-rails/sidekiq.log\",\"type\":\"phrase\"}}},\"$state\":{\"store\":\"appState\"}}]}"
            }
          }
        },
        {
          "id": @index.to_s,
          "type": 'index-pattern',
          "updated_at": '2019-09-10T17:00:53.191Z',
          "version": 12,
          "attributes": {
            "title": 'logstash-*',
            "timeFieldName": 'time',
            "fields": '[{"name":"@timestamp","type":"date","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"@version","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"_id","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":false},{"name":"_index","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":false},{"name":"_score","type":"number","count":0,"scripted":false,"searchable":false,"aggregatable":false,"readFromDocValues":false},{"name":"_source","type":"_source","count":0,"scripted":false,"searchable":false,"aggregatable":false,"readFromDocValues":false},{"name":"_type","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":false},{"name":"action","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"action.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"backtrace","type":"number","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"category","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"category.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"class","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"class.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"completed_at","type":"date","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"controller","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"controller.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"correlation_id","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"correlation_id.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"created_at","type":"date","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"db","type":"number","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"dead","type":"boolean","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"default-hierarchy","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"default-hierarchy.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"duration","type":"number","count":1,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"enqueued_at","type":"date","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"format","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"format.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"geoip.ip","type":"ip","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"geoip.latitude","type":"number","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"geoip.location","type":"geo_point","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"geoip.longitude","type":"number","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"gitaly_calls","type":"number","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"gitaly_duration","type":"number","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"go.version","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"go.version.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"grpc.code","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"grpc.code.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"grpc.meta.auth_version","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"grpc.meta.auth_version.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"grpc.meta.client_name","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"grpc.meta.client_name.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"grpc.method","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"grpc.method.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"grpc.request.deadline","type":"date","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"grpc.request.fullMethod","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"grpc.request.fullMethod.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"grpc.request.glProjectPath","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"grpc.request.glProjectPath.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"grpc.request.glRepository","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"grpc.request.glRepository.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"grpc.request.repoPath","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"grpc.request.repoPath.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"grpc.request.repoStorage","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"grpc.request.repoStorage.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"grpc.request.topLevelGroup","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"grpc.request.topLevelGroup.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"grpc.service","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"grpc.service.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"grpc.start_time","type":"date","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"grpc.time_ms","type":"number","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"host","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"host.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"http.request.host","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"http.request.host.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"http.request.id","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"http.request.id.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"http.request.method","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"http.request.method.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"http.request.remoteaddr","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"http.request.remoteaddr.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"http.request.uri","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"http.request.uri.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"http.request.useragent","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"http.request.useragent.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"http.response.contenttype","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"http.response.contenttype.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"http.response.duration","type":"string","count":2,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"http.response.duration.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"http.response.status","type":"number","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"http.response.written","type":"number","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"ip","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"ip.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"jid","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"jid.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"job_status","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"job_status.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"level","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"level.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"location","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"location.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"message","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"method","type":"string","count":1,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"method.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"msg","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"msg.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"path","type":"string","count":1,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"path.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"peer.address","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"peer.address.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"pid","type":"number","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"port","type":"number","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"queue","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"queue.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"queue_duration","type":"number","count":1,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"queue_namespace","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"queue_namespace.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"remote_ip","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"remote_ip.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"retry","type":"boolean","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"route","type":"string","count":1,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"route.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"rugged_calls","type":"number","count":1,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"rugged_duration_ms","type":"number","count":1,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"scheduling_latency_s","type":"number","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"severity","type":"string","count":1,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"severity.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"source","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"source.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"span.kind","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"span.kind.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"stamp","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"stamp.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"status","type":"number","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"storage","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"storage.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"system","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"system.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"time","type":"date","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"ua","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"ua.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"user_id","type":"number","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"username","type":"string","count":1,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"username.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"vars.name","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"vars.name.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"vars.reference","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"vars.reference.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"view","type":"number","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true},{"name":"wrapped","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":false,"readFromDocValues":false},{"name":"wrapped.keyword","type":"string","count":0,"scripted":false,"searchable":true,"aggregatable":true,"readFromDocValues":true}]'
          }
        },
        {
          "id": '572cafa0-3bb0-11e9-8c37-cb7fed333dd2',
          "type": 'dashboard',
          "updated_at": '2019-09-10T17:15:17.021Z',
          "version": 4,
          "attributes": {
            "title": 'Display',
            "hits": 0,
            "description": '',
            "panelsJSON": '[{"embeddableConfig":{},"gridData":{"x":0,"y":31,"w":24,"h":15,"i":"1"},"id":"e7abc7d0-3ba8-11e9-8c37-cb7fed333dd2","panelIndex":"1","type":"visualization","version":"6.6.1"},{"embeddableConfig":{},"gridData":{"x":0,"y":70,"w":24,"h":15,"i":"2"},"id":"e4aa47a0-3ba8-11e9-8c37-cb7fed333dd2","panelIndex":"2","type":"visualization","version":"6.6.1"},{"embeddableConfig":{},"gridData":{"x":24,"y":46,"w":24,"h":24,"i":"4"},"id":"e6778d90-3ba8-11e9-8c37-cb7fed333dd2","panelIndex":"4","type":"visualization","version":"6.6.1"},{"embeddableConfig":{},"gridData":{"x":0,"y":46,"w":24,"h":24,"i":"5"},"id":"e5de5ad0-3ba8-11e9-8c37-cb7fed333dd2","panelIndex":"5","type":"visualization","version":"6.6.1"},{"embeddableConfig":{"vis":{"params":{"sort":{"columnIndex":null,"direction":null}}}},"gridData":{"x":0,"y":116,"w":48,"h":41,"i":"6"},"id":"e405f150-3ba8-11e9-8c37-cb7fed333dd2","panelIndex":"6","type":"visualization","version":"6.6.1"},{"embeddableConfig":{"vis":{"params":{"sort":{"columnIndex":null,"direction":null}}}},"gridData":{"x":0,"y":85,"w":48,"h":31,"i":"7"},"id":"e2c72fc0-3ba8-11e9-8c37-cb7fed333dd2","panelIndex":"7","type":"visualization","version":"6.6.1"},{"embeddableConfig":{},"gridData":{"x":24,"y":31,"w":24,"h":15,"i":"8"},"id":"e8e9ed20-3ba8-11e9-8c37-cb7fed333dd2","panelIndex":"8","type":"visualization","version":"6.6.1"},{"embeddableConfig":{},"gridData":{"x":24,"y":70,"w":24,"h":15,"i":"9"},"id":"e54416a0-3ba8-11e9-8c37-cb7fed333dd2","panelIndex":"9","type":"visualization","version":"6.6.1"},{"embeddableConfig":{},"gridData":{"x":0,"y":157,"w":48,"h":13,"i":"10"},"id":"c8c393c0-41d9-11e9-b300-9f3a55703eb8","panelIndex":"10","type":"visualization","version":"6.6.1"},{"embeddableConfig":{},"gridData":{"x":0,"y":16,"w":48,"h":15,"i":"11"},"id":"248fdf90-41dc-11e9-b300-9f3a55703eb8","panelIndex":"11","type":"visualization","version":"6.6.1"},{"embeddableConfig":{"vis":{"legendOpen":true}},"gridData":{"x":0,"y":170,"w":48,"h":14,"i":"12"},"id":"96e165d0-41de-11e9-b300-9f3a55703eb8","panelIndex":"12","type":"visualization","version":"6.6.1"},{"embeddableConfig":{},"gridData":{"x":0,"y":0,"w":48,"h":16,"i":"13"},"id":"cf7c3ba0-41f1-11e9-b300-9f3a55703eb8","panelIndex":"13","type":"visualization","version":"6.6.1"},{"embeddableConfig":{},"gridData":{"x":0,"y":184,"w":48,"h":11,"i":"14"},"id":"4f400dc0-41fd-11e9-b31f-bdfff4e56f4d","panelIndex":"14","type":"visualization","version":"6.6.1"},{"embeddableConfig":{},"gridData":{"x":4,"y":255,"w":42,"h":24,"i":"15"},"id":"6072ff80-c439-11e9-9d80-2b20017a3c16","panelIndex":"15","type":"visualization","version":"6.6.1"},{"embeddableConfig":{},"gridData":{"x":24,"y":195,"w":24,"h":15,"i":"16"},"id":"e21e1b70-d3eb-11e9-9313-01e5c8765727","panelIndex":"16","type":"visualization","version":"6.6.1"},{"embeddableConfig":{},"gridData":{"x":0,"y":195,"w":24,"h":15,"i":"17"},"id":"5b891090-d3ed-11e9-9313-01e5c8765727","panelIndex":"17","type":"visualization","version":"6.6.1"},{"embeddableConfig":{},"gridData":{"x":24,"y":210,"w":24,"h":15,"i":"18"},"id":"b8811d10-d3ed-11e9-9313-01e5c8765727","panelIndex":"18","type":"visualization","version":"6.6.1"},{"embeddableConfig":{},"gridData":{"x":0,"y":210,"w":24,"h":15,"i":"19"},"id":"1bfbe140-d3ee-11e9-9313-01e5c8765727","panelIndex":"19","type":"visualization","version":"6.6.1"},{"embeddableConfig":{},"gridData":{"x":24,"y":225,"w":24,"h":15,"i":"20"},"id":"c788e190-d3ec-11e9-9313-01e5c8765727","panelIndex":"20","type":"visualization","version":"6.6.1"},{"gridData":{"x":0,"y":225,"w":24,"h":15,"i":"21"},"version":"6.6.1","panelIndex":"21","type":"visualization","id":"d51e0aa0-d3e8-11e9-8369-c5395322e794","embeddableConfig":{}},{"gridData":{"x":24,"y":240,"w":24,"h":15,"i":"22"},"version":"6.6.1","panelIndex":"22","type":"visualization","id":"90c52270-d3e9-11e9-8369-c5395322e794","embeddableConfig":{}},{"gridData":{"x":0,"y":240,"w":24,"h":15,"i":"23"},"version":"6.6.1","panelIndex":"23","type":"visualization","id":"e2db14c0-d3e9-11e9-8369-c5395322e794","embeddableConfig":{}}]',
            "optionsJSON": '{"darkTheme":false,"hidePanelTitles":false,"useMargins":true}',
            "version": 1,
            "timeRestore": true,
            "timeTo": 'now',
            "timeFrom": 'now-7d',
            "refreshInterval": {
              "pause": true,
              "value": 0
            },
            "kibanaSavedObjectMeta": {
              "searchSourceJSON": '{"query":{"language":"lucene","query":""},"filter":[]}'
            }
          }
        }
      ]
    }
  end
end
# rubocop:enable Metrics/LineLength, Metrics/MethodLength, Metrics/ModuleLength
