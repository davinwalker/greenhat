# Top level namespace
module GreenHat
  # CLI Methods
  module Cli
    def self.color(*splat)
      puts Pastel.new.decorate(*splat)
    end

    def self.compose_path
      "#{__dir__}/compose/docker-compose.yml"
    end

    def self.list
      @list ||= List.new
      # puts @list.inspect
      @list
    end

    def self.docker_start
      puts Pastel.new.decorate('Starting ELK', :green)
      system("docker-compose -p greenhat --file #{compose_path} down")
      system("docker-compose -p greenhat --file #{compose_path} up -d")
    end

    def self.docker_stop
      puts Pastel.new.decorate('Stopping ELK', :red)
      system("docker-compose -p greenhat --file #{compose_path} down")
    end

    def self.start(files)
      missing if files.empty? || files.first&.include?('-h')

      files.each do |archive|
        bad_file(archive) unless File.exist? archive
      end

      docker_start
      Elasticsearch.status

      files.each do |file|
        host = Host.new
        host.archive = file
        host.files = Archive.load(file)
        list.hosts.push host
      end

      Kibana.create
      color('Check out: http://localhost:5601/app/kibana', :green, :bold)

      # Write Out Summary
      File.write(output, Slim::Template.new(template).render(list))
      puts "Summary/Info - file://#{File.expand_path('greenhat.html')}"

      interactive
    end

    def self.template
      "#{__dir__}/views/index.slim"
    end

    def self.output
      'greenhat.html'
    end

    def self.prompt
      TTY::Prompt.new(active_color: :cyan)
    end

    def self.menu
      prompt.select('Wat do?') do |menu|
        menu.choice name: 'exit'
        menu.choice name: 'Export Kibana Dashboard', value: 'export'
        menu.choice name: 'console (pry)', value: 'console'
      end
    end

    def self.interactive
      case menu
      when 'exit'
        docker_stop
        exit(0)
      when 'export' then export
      when 'console' then console
      end

      interactive
    rescue TTY::Reader::InputInterrupt
      exit 0
    end

    def self.export
      Kibana.export_dashboard
    end

    def self.console
      require 'pry'
      binding.pry
    end

    def self.bad_file(archive)
      puts color("Cannot find archive: #{archive}", :red)
      exit 1
    end

    def self.missing
      puts color('Missing Archive', :red)
      puts 'Usage: greenhat  <sos-archive.tgz> <sos-archive2.tgz>'
      exit 1
    end
  end
end
