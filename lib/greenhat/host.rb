# Simple class to represent an environment
class Host
  include ActionView::Helpers::DateHelper
  include ActionView::Helpers::NumberHelper

  attr_accessor :archive, :files, :uuid

  def initialize
    super

    # Used for Jquery objects
    @uuid = SecureRandom.uuid
  end

  def archive_name
    File.basename archive
  end

  def find_file(file_name)
    files.find { |x| x.name == file_name }
  end

  def icon
    release_file = find_file files.map(&:name).grep(/_release/).first

    release = release_file.raw.join

    if release.include? 'ubuntu'
      'fa-ubuntu'
    elsif release.include? 'suse'
      'fa-suse'
    elsif release.include? 'redhat'
      'fa-redhat'
    elsif release.include? 'centos'
      'fa-centos'
    else
      'fa-linux'
    end
  end

  # ----------------------------
  # Helpers
  # ----------------------------
  def percent(value, total)
    (value.to_i / total.to_f).round(2) * 100
  end

  def systemctl_color(entry)
    case entry.status
    when 'enabled'  then :green
    when 'static'   then :orange
    when 'disabled' then :red
    else
      :grey
    end
  end
  # ---------------------------

  def manifest
    file = find_file 'gitlab_version_manifest_json'
    return nil unless file

    Oj.load file.raw.join
  end

  def uptime
    file = find_file 'uptime'
    return nil unless file

    file.raw
  end

  def uname
    file = find_file 'uname'
    return nil unless file

    file.raw.join
  end

  def cpuinfo
    file = find_file 'cpuinfo'
    return nil unless file

    file.raw.join("\n").split("\n\n").map do |cpu|
      all = cpu.split("\n").map do |row|
        row.delete("\t").split(': ')
      end
      { details: all[1..-1], order: all[0].last }
    end
  end

  def cpu_speed
    file = find_file 'lscpu'
    return nil unless file

    details = file.raw.find { |x| x.include? 'MHz' }
    details.reverse.split(' ', 2).map(&:reverse).reverse
  end

  def total_memory
    file = find_file 'free_m'
    return nil unless file

    value = file.raw.dig(1, 1).to_i
    number_to_human_size(value * 1024 * 1024)
  end

  def free_m
    file = find_file 'free_m'
    return nil unless file

    file.raw
  end

  def df_h
    file = find_file 'df_h'
    return nil unless file

    file.raw
  end

  def netstat
    file = find_file 'netstat'
    return nil unless file

    file.raw
  end

  def ulimit
    file = find_file 'ulimit'
    return nil unless file

    results = file.raw.map do |entry|
      {
        value: entry.split(' ')[-1],
        details: entry.split('  ').first
      }
    end

    results.sort_by { |x| x[:details].downcase }
  end

  def processes
    file = find_file 'ps'
    return nil unless file

    headers = file.raw.first.split(' ', 11)
    list = file.raw[1..-1].each.map do |row|
      row.split(' ', 11).each_with_index.each_with_object({}) do |(v, i), obj|
        obj[headers[i]] = v
      end
    end
    { headers: headers, list: list }
  end

  def systemctl_unit_files
    file = find_file 'systemctl_unit_files'
    return nil unless file

    all = file.raw[1..-2].map do |x|
      unit, status = x.split(' ')
      { unit: unit, status: status }
    end

    all.reject! { |x| x[:unit].nil? }
    all.sort_by(&:unit)
  end
end


# Slim Wrapper
class List
  attr_accessor :hosts

  def initialize
    self.hosts = []
  end

  def slim(file, host = nil)
    Slim::Template.new("#{__dir__}/views/#{file}.slim").render(host)
  end
end
